﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathTrigger : MonoBehaviour // got this on the youtube at https://youtu.be/nBgCeJBMT0k
{
    [SerializeField] private Transform player;
    [SerializeField] private Transform respawnPoint;
    // Start is called before the first frame update
    void OnTriggerEnter2D(Collider2D other)
    {
        player.transform.position = respawnPoint.transform.position; // all it does is respawn you
    }
}
