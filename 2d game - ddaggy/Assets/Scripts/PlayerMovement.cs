﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerMovement : MonoBehaviour
{

    Rigidbody2D rB2D;

    public float runSpeed;
    public float jumpForce;
    public float count;

    public SpriteRenderer spriteRenderer;

    public Animator animator;
    public GameObject deathTrigger;

    public TextMeshProUGUI countText;
    public GameObject finishText;

    //public Transform player;
    //public Transform spawnPoint;

    // Start is called before the first frame update
    void Start()
    {
        rB2D = GetComponent<Rigidbody2D>();
        count = 0;
        finishText.SetActive(false);

        SetCountText();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump")) {
            int levelMask = LayerMask.GetMask("Level");

            if (Physics2D.BoxCast(transform.position, new Vector2(1f, 1f), 0f, Vector2.down, .01f, levelMask))
            {
                Jump();
            }
        }
    }

    void SetCountText()
    {
        countText.text = "Score: " + count.ToString();
    }

    //public void RespawnPlayer()
    //{
    //    Instantiate(player, spawnPoint.position, spawnPoint.rotation);
    //}

    void OnTriggerEnter2D(Collider2D other)
    {
        //if (other.gameObject.CompareTag("Death"))
        //{
        //    Debug.Log("You died");
        //}

        if (other.gameObject.CompareTag("CollectorItem"))
        {
            other.gameObject.SetActive(false);
            count += 100;

            SetCountText();
        }

        if (other.gameObject.CompareTag("Finish"))
        {
            finishText.SetActive(true);
            Invoke("QuitGame", 5.0f);
        }
    }

    void FixedUpdate()
    {
        // Run from left to right
        float horizontalInput = Input.GetAxis("Horizontal");

        rB2D.velocity = new Vector2(horizontalInput * runSpeed * Time.deltaTime, rB2D.velocity.y);
        
        if (rB2D.velocity.x > 0) // flipping the sprite so it runs in the direction it makes 
        {
            spriteRenderer.flipX = false;
        } 
        else if (rB2D.velocity.x < 0)
        {
            spriteRenderer.flipX = true;
        }

        if (Mathf.Abs(horizontalInput) > 0f) // for the idle and run functions
        {
            animator.SetBool("IsRunning", true);
        }
        else
        {
            animator.SetBool("IsRunning", false);
        }
    }

    void Jump()
    {
        rB2D.velocity = new Vector2(rB2D.velocity.x, jumpForce);
    }

    void QuitGame()
    {
        Application.Quit();
    }
}
